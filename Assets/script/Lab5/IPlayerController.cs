using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Teerakarn.GameDev3.chapter5
{
    public interface IPlayerController
     {
         void MoveForward();
         void MoveForwardSprint();
        
         void MoveBackward();
        
         void TurnLeft();
         void TurnRight();
         }
}