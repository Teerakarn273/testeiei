﻿using System;
using UnityEngine;

namespace Teerakarn.GameDev3.chapter5
{
    public class AlwaysFaceCamera : MonoBehaviour
    {
        private void Update()
        {
            transform.rotation = 
                Quaternion.LookRotation( transform.position - Camera.main.transform.position );
        }
    }
}