using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Teerakarn.GameDev3.chapter5
{
    public interface IActorEnterExitHandler
     {
         void ActorEnter(GameObject actor);
         void ActorExit(GameObject actor);
     }
}