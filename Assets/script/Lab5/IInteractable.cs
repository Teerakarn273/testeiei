using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Teerakarn.GameDev3.chapter5
{
    public interface IInteractable
     {
         void Interact(GameObject actor);
     }

}