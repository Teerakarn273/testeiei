using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Teerakarn.GameDev3.chapter9
{
    [CreateAssetMenu(menuName = "GameDev3/Util/ScriptableObjectUtils")]
    public class ScriptableObjectUtils :  ScriptableObject
    {
        public void Destroy(GameObject gameObject)
             {
             Object.Destroy(gameObject);
             }
    }
}