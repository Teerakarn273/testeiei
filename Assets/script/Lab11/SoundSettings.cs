using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;

using UnityEngine;

namespace Teerakarn.GameDev3.chapter11
{
    [CreateAssetMenu(menuName = "GameDev3/Chapter11/SoundSettingsPreset",fileName = "SoundSettingsPreset")]
    public class SoundSettings : ScriptableObject
    {
        public AudioMixer AudioMixer;
            
             [Header("MasterVolume")]
         public string MasterVolumeName = "Master Volume";
         [Range(-80,20)]
         public float MasterVolume;
            
             [Header("MusicVolume")]
         public string MusicVolumeName = "Music Volume";
         [Range(-80,20)]
         public float MusicVolume;
            
             [Header("MasterSFXVolume")]
         public string MasterSFXVolumeName = "Master SFX Volume";
         [Range(-80,20)]
         public float MasterSFXVolume;
            
             [Header("SFXVolume")]
         public string SFXVolumeName = "SFX Volume";
         [Range(-80,20)]
         public float SFXVolume;
            
             [Header("UIVolume")]
         public string UIVolumeName = "UI Volume";
         [Range(-80,20)]
         public float UIVolume;
    }
}