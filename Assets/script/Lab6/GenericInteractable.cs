using Teerakarn.GameDev3.chapter5;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;



namespace Teerakarn.GameDev3.chapter6
{
    public class GenericInteractable : MonoBehaviour, IInteractable, IActorEnterExitHandler
    {
        [SerializeField] protected UnityEvent m_OnInteract = new();
        [SerializeField] protected UnityEvent m_OnActorEnter = new();
        [SerializeField] protected UnityEvent m_OnActorExit = new();
        
        [SerializeField] protected UnityEvent<GameObject> m_OnInteractGameobject = new();
        [SerializeField] protected UnityEvent<GameObject> m_OnActorEnterGameobject = new();
        [SerializeField] protected UnityEvent<GameObject> m_OnActorExitGameobject = new();

        public virtual void Interact(GameObject actor)
             {
             m_OnInteract.Invoke();
             m_OnInteractGameobject.Invoke(actor);
             }
    
     public virtual void ActorEnter(GameObject actor)
     {
         m_OnActorEnter.Invoke();
         m_OnActorEnterGameobject.Invoke(actor);
     }
    
     public virtual void ActorExit(GameObject actor)
     {
         m_OnActorExit.Invoke();
         m_OnActorExitGameobject.Invoke(actor);
     }
        
        
    }
    
}