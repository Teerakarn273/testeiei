using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


namespace Teerakarn.GameDev3.chapter5
{
    [RequireComponent(typeof(Rigidbody))]
    public class Pushing : MonoBehaviour
    {
        [SerializeField] protected TextMeshProUGUI m_InteractionTxt;
        [SerializeField] protected float m_Power = 10;
        private Rigidbody m_rigidBody;
    
        private void Start()
        {
            m_rigidBody = GetComponent <Rigidbody >();
        }
    
        public void Interact(GameObject actor)
        {
            m_rigidBody.AddForce(actor.transform.forward*m_Power,ForceMode.
                Impulse);
        }

    }
}